<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach(range(1,5) as $index)
        {
            DB::table('books')->insert([
            'isbn'      => $faker->isbn10,
            'title'     => $faker->sentence($nbWords = 3),
            'author'    => $faker->name,
            'publisher' => $faker->company.' Press',
            'image'     => $faker->word.'01,',
            ]);
        }
    }
}

/*
            $table->string('isbn',100);
            $table->string('title',200);
            $table->string('author',200);
            $table->string('publisher',200);
            $table->string('image',45);
*/