<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $books=Book::all();
        return view('books.index')->withBooks($books);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'isbn'=> 'required',
            'title'=> 'required',
            'author'=> 'required',
            'publisher'=> 'required',
        ]);

        $input = $request->all();

        Book::create($input);

        Session::flash('flash_message', 'Task successfully added!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    public function show($id)
    {
        $book = Book::findOrFail($id);

        return view('books.show')->withBook($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        return view('books.edit')->withBook($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);

        $this->validate($request, [
            'isbn'=> 'required',
            'title'=> 'required',
            'author'=> 'required',
            'publisher'=> 'required',
        ]);
        $file = $request->file('image');

        $var = public_path().'/img/';

        if ($file->move($var, $file->getClientOriginalName() )) {
    echo 'uploaded';} else {echo 'FAILED'; };

        $input = $request->all();

        $book->fill($input)->save();

        $imageName = $request->file('image')->getClientOriginalName() . $request->file('image')->getClientOriginalExtension();

        $request->file('image')->move('app/public/img/',$imageName);

        Session::flash('flash_message', 'Book successfully added!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);

        $book->delete();

        Session::flash('flash_message', 'Book successfully deleted!');

        return redirect()->route('books.index');
    }
}
