@extends('layout.master')
@section('content')
@include('partial.errors')

<h1>Update Book</h1>

{!! Form::model($book,[
    'method' => 'PATCH', 
    'route' => ['books.update', $book->id]],
    'files => true'
) !!}
<form class="form-horizontal">
    <div class="form-group">
        <label for="image" class="col-sm-2 control-label">Cover</label>
        <div class="col-sm-10">
            <img src="{{ asset('img/'.$book->image)}}" height ="180" width ="150" class="img-rounded">
        </div>
    </div>
    <div class="form-group">
            {!! Form::label('ISBN','ISBN:',['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('isbn',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
            {!! Form::label('Title','Title:',['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('title',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
            {!! Form::label('Author','Author:',['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('author',null,['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
            {!! Form::label('Publisher','Publisher:',['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('publisher',null,['class'=>'form-control']) !!}
        </div>
<!--</div>
        <div class="form-group">
            {!! Form::label('Image','Image:',['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('image',null,['class'=>'form-control']) !!}
        </div>
    </div>-->
    <div class="form-group">
        <div class="col-sm-10">
            {!! Form::label('Image','Image:') !!}

            {!! Form::file('image',null,['class'=>'form-control']) !!}
        </div>
    </div>
    
{!! Form::submit('Update Book', ['class' => 'btn btn-primary form-control']) !!}
{!! Form::close() !!}
</form>

@stop