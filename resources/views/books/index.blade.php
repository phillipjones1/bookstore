@extends('layout.master')

@section('content')

<h1>Phillip's Bookstore</h1>
<p class="lead">Book Catalog. <a href="  {{ route('books.create') }}">Add Book</a></p>
<hr>

<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr class="bg-info">
        <th>Id</th>
        <th>ISBN</th>
        <th>Title</th>
        <th>Author</th>
        <th>Publisher</th>
        <th>Thumbs</th>
        <th colspan="3">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>{{ $book->isbn }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->author }}</td>
            <td>{{ $book->publisher }}</td>
            <td>
                <img src="{{ asset('img/'. $book->image)}}" height="35" width="30">
            </td>
            <td>
                <a href="{{ route('books.show', $book->id)}}" class="btn btn-primary">Read</a>
            </td>
            <td>
                <a href="{{ route('books.edit', $book->id)}}" class="btn btn-warning">Update</a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['books.destroy', $book['id']]]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@stop